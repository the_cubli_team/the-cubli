/*********************************************************************************************************
  PROJET EI4
  TheCubli
  LE 10/02/2017
                                                                                                 BY TEXIER
*********************************************************************************************************/



#include <SPI.h>
#include "mcp_can.h"
#include <Keyboard.h>
#include <Servo.h>
#include "Keyboard.h"


// Attributs

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

const int tailletableau = 8;
int gyro[8];

byte _vitesse[3] = {0, 0x06, 0x20};
byte _stop[3] = {0x00, 0x00, 0x00};
byte _inversetest[3] = {0x01, 0x04, 0x20};
const byte _init[1] = {0x01};

// the cs pin of the version after v1.1 is default to D9
// v0.9b and v1.0 is default D10
const int SPI_CS_PIN = 9;
MCP_CAN CAN(SPI_CS_PIN);                                    // Set CS pin

char inChar = ' ';
char extChar = ' ';
int etat=0;

void setup() {

    // Test BUS CAN
    Serial.begin(115200);

    while (CAN_OK != CAN.begin(CAN_500KBPS))              // init can bus : High Speed Can = 125k
    {
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
        delay(100);
    }
    Serial.println("CAN BUS Shield init ok!");

    myservo.writeMicroseconds(1500);//sert a initialiser la position du servomoteur    

    // Tests I2C

    // Initialisation des variables

         // Servo
    myservo.attach(9);  // attaches the servo on pin 9 to the servo object
    initialisation();   //appel de la focntion qui init le moteur
    
    //inertial(gyro, tailletableau);
    //affichetableau(gyro, tailletableau);

    return 0;
}

bool first = false;

void loop() 
    first = false;
    if(Serial.available()>0)
    {
      extChar=inChar;
      inChar=Serial.read();
      if(extChar != inChar)first = true;
    }
    switch(inChar)
    {
      case 's':
        if(first)
        {
          arret();
          inChar='c';
          _vitesse[0]=1;
          avance();
        }
      break;
      
      case 'r':
        if(first)
        {
          _vitesse[0]=0;
          avance();
        }
      break;
      case 'c':
        if(first)
        {
          _vitesse[0]=1;
          avance();
        }
        delay(500);
      break;

      case 'k':
        if(first) arret;
      break
    }
}

/*
    *   Programme qui actionne le servo pour freiner
    *   @arg1 : position du servo pour freiner la roue
    *   return void;
*/

void FreinServo(int pos)
{

    myservo.writeMicroseconds(pos);
    delay(200);
    myservo.writeMicroseconds(1500);
    delay(200);
}

/*
    *   Programme qui fait tourner la roue    
    *   return void;
*/
void avance()
{
    Serial.println("Changement de vitesse");
    CAN.sendMsgBuf(0x03,0, 3, _vitesse);
    Serial.println("Fin changement de vitesse");  
}
/*
    *   Fonction pour faire freiner la roue
    *   utilise la fonction FreinServo pour freiner plus vite
    *   return void;
    */
void arret()
{   
     
    //Arret Moteur
    Serial.println("___stop___");
    CAN.sendMsgBuf(0x03,0, 3, _stop);
    Serial.println("Sending Message"); 
    //delay(10);
    FreinServo(1150);  
    
}
    /*
    *   Programme pour recupérer les valeurs de la centrale
    *   @arg1 : pointeur vers le tableau gyro
    *   @arg2 : la taille du tableau
    *   return void;
    */
void inertial(int *tableau, int tailleTableau)
{
    //.....
}
    /*
    *   Programme pour initialiser la carte moteur
    *   return void;
    */
void initialisation()
{
    Serial.println("___initialisation___");
    CAN.sendMsgBuf(0x04,0, 1, _init);
    Serial.println("Sending Message");  
}

    /*
    *   Fonction pour afficher un tableau
    *   @arg1 : pointeur vers le tableau à afficher
    *   @arg2 : la taille du tableau
    *   return void;
    */
void affichetableau(int *tableau, int tailleTableau)
{
    int i;
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf("%d\n", tableau[i]);
    }
}


/*********************************************************************************************************
  END FILE
*********************************************************************************************************/

