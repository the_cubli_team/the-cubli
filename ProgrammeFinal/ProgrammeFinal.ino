/*********************************************************************************************************
  PROJET EI4
  TheCubli
  LE 10/02/2017
                                                                                                 BY TEXIER
*********************************************************************************************************/



#include <SPI.h>
#include "mcp_can.h"
#include <Keyboard.h>
#include <Servo.h>




// Attributs

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards
int _positionServo = 0;    // variable to store the servo position


const int tailletableau = 8;
int gyro[8];

byte _vitesse[3] = {0, 0x04, 0x20};
byte _stop[3] = {0x00, 0x00, 0x00};
byte _inversetest[3] = {0x01, 0x04, 0x20};
const byte _init[1] = {0x01};



// the cs pin of the version after v1.1 is default to D9
// v0.9b and v1.0 is default D10
const int SPI_CS_PIN = 9;
MCP_CAN CAN(SPI_CS_PIN);                                    // Set CS pin





void setup() {


    // Test BUSCAN
    Serial.begin(115200);

    while (CAN_OK != CAN.begin(CAN_500KBPS))              // init can bus : High Speed Can = 125k
    {
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
        delay(100);
    }
    Serial.println("CAN BUS Shield init ok!");



    // Tests I2C

    // Initialisation des variables

         // Servo
    myservo.attach(9);  // attaches the servo on pin 9 to the servo object
    initialisation();
    
        // Initialisation de la carte moteur
        // On affiche le contenu du tableau
    //inertial(gyro, tailletableau);
    //affichetableau(gyro, tailletableau);

    
    return 0;
}



void loop() {
    avance();
    delay(3000);
    arret();
    delay(3000);
}


void BougeServo1(int posMax)
{
  int posact = myservo.read();
  for (int pos = posact; pos <= posMax; pos += 1) // goes from 0 degrees to 180 degrees
  { 
    myservo.write(pos);
    delay(5); 
  }
  
}


void BougeServo2(int posMax)
{
  int posact = myservo.read();
  
  for (int pos = posact; pos < posMax; pos ++)
  { 
    myservo.write(-pos);
    delay(15); 
    Serial.println("servobouge");  
  }
  
}



void avance()
{
    Serial.println("Changement de vitesse");
    CAN.sendMsgBuf(0x03,0, 3, _vitesse);
    Serial.println("Fin changement de vitesse");  
}



void arret()
{
    //Arret Moteur
    Serial.println("___stop___");
    CAN.sendMsgBuf(0x03,0, 3, _stop);
    Serial.println("Sending Message"); 
    //delay(10);
    BougeServo1(60);
    
}



    /*
    *   Programme de Quentin pour recupérer les valeurs de la centrale
    *   @arg1 : pointeur vers le tableau gyro
    *   @arg2 : la taille du tableau
    *   return void;
    */
void inertial(int *tableau, int tailleTableau)
{

}



    /*
    *   Programme pour initialiser de la carte moteur 
    *   return void;
    */
void initialisation()
{
    Serial.println("___initialisation___");
    CAN.sendMsgBuf(0x04,0, 1, _init);
    Serial.println("Sending Message");  
}

    /*
    *   Fonction pour afficher un tableau
    *   @arg1 : pointeur vers le tableau à afficher
    *   @arg2 : la taille du tableau
    *   return void;
    */
void affichetableau(int *tableau, int tailleTableau)
{
    int i;
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf("%d\n", tableau[i]);
    }
}


/*********************************************************************************************************
  END FILE
*********************************************************************************************************/

