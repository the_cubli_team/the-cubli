/* Sweep
 by BARRAGAN <http://barraganstudio.com>
 This example code is in the public domain.

 modified 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Sweep
*/

#include <Servo.h>

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 90;    // variable to store the servo position

void setup() {
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  myservo.write(90);
  Serial.println("___setup___");
  
}

void loop() {

    delay(500);
    bougeantihoraire(30);
    Serial.println("___antiH___");
    delay(500);
    bougehoraire(95);
    Serial.println("___Horaire___");
  
}


void bougehoraire(int posfinale)
{
  int i=0;
  for (i = pos; i <= posfinale; i += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(i);              // tell servo to go to position in variable 'pos'
    delay(0);                       // waits 15ms for the servo to reach the position
  }
  pos=i;
}

void bougeantihoraire(int posfinale)
{
  int i=0;
  for (i = pos; i >= posfinale; i -= 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(i);              // tell servo to go to position in variable 'pos'
    delay(0);                       // waits 15ms for the servo to reach the position
  }
  pos=i;
}

