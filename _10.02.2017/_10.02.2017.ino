/*********************************************************************************************************
  PROJET EI4
  PROGRAMME DE BASE TEST
  LE 04/01/2017
                                                                                                 BY TEXIER
*********************************************************************************************************/



#include <SPI.h>
#include "mcp_can.h"
#include <Keyboard.h>


// the cs pin of the version after v1.1 is default to D9
// v0.9b and v1.0 is default D10
const int SPI_CS_PIN = 9;

MCP_CAN CAN(SPI_CS_PIN);                                    // Set CS pin


byte  avance_test[3] = {0, 0x04, 0x20};
byte stop_test[3] = {0x00, 0x00, 0x00};
byte inverse_test[3] = {0x01, 0x04, 0x20};
byte init_test[1] = {0x01};
int gyro[8];

void setup()
{
    Serial.begin(115200);

    while (CAN_OK != CAN.begin(CAN_500KBPS))              // init can bus : High Speed Can = 125k
    {
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
        delay(100);
    }
    Serial.println("CAN BUS Shield init ok!");

    Serial.println("___initialisation___");
    CAN.sendMsgBuf(0x04,0, 1, init_test);
    Serial.println("Sending Message");



    Serial.println("___avance___");
    CAN.sendMsgBuf(0x03,0, 3, avance_test);
    Serial.println("Sending Message");   


    delay(3000);
    
    /*Serial.println("___stop___");
    CAN.sendMsgBuf(0x03,0, 3, stop_test);
    Serial.println("Sending Message");

    delay(50);*/
    
    Serial.println("___Changement de sens___");
    CAN.sendMsgBuf(0x03,0, 3, inverse_test);
    Serial.println("Sending Message");

    delay(3000);
    
    Serial.println("___stop___");
    CAN.sendMsgBuf(0x03,0, 3, stop_test);
    Serial.println("Sending Message");

}


void loop()
{
    unsigned char len = 0;
    unsigned char buf[8];

    if(CAN_MSGAVAIL == CAN.checkReceive())            // check if data coming
    {
        CAN.readMsgBuf(&len, buf);    // read data,  len: data length, buf: data buf

        unsigned char canId = CAN.getCanId();

        Serial.println("-----------------------------");
        Serial.println("get data from ID: ");
        Serial.println(canId);

        for(int i = 0; i<len; i++)    // print the data
        {
            Serial.print(buf[i]);
            Serial.print("\t");            
        }
        Serial.println();
    }
}




/*********************************************************************************************************
  END FILE
*********************************************************************************************************/

